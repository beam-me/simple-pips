# SIMPLE-PIPS

The SIMPLE-PIPS model is specifically tailored for the usage with the [parallel interior point solver PIPS-IPM++](https://github.com/NCKempke/PIPS-IPMpp).
In addition to minor changes in the model structure compared to the other SIMPLE models, it implements various model 
annotations and several ways to generate the model in a format readable by the GAMS/PIPS-IPM++-Link.

1. [The SIMPLE model](#the-simple-model)
    1. [Building the annotated gdx file](#building-the-annotated-gdx-file)
    2. [Solving the problem with PIPS-IPM++](#solving-the-problem-with-pips-ipm)
    3. [Block-wise generation of the gdx files](#block-wise-generation-of-the-gdx-files)
2. [Authors](#authors)
3. [Acknowledgements](#acknowledgements)

> SIMPLE-PIPS also includes a simplified approach for distributed block generation in the branch limVarDom. This change was implemented after the end of the BEAM-ME project and is therefore not described in the best practice guide in detail. This feature requires the GAMS version 31 or above.


## The SIMPLE model

The SIMPLE model is a highly simplified representation of an optimizing energy system model (OESM) developed during
the BEAM-ME project. The model adressess the need to have a common ground to discuss speed-up methods for complex
ESMs. This is achieved by preserving relevant parts of the model structure that can be found in many complex ESMs 
but at the same time having a compact and comprehensive source code.

All SIMPLE models come with a data generator simple\_data\_ gen.gms that can be parametrized to generate data instances
of different size. The data generator is called automatically from the main model and takes only the number of regions
as an argument to scale the problem size. All the data is computed by randomizing standard basic time series that 
provide the corresponding data for a model region.

Data sources:
- **timeSeries.csv**
  - demand: [Actual electricity consumption in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:2,%22selectedSubCategory%22:5,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - photovoltaic: [Actual electricity generation in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:1,%22selectedSubCategory%22:1,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - wind: [Actual electricity generation in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:1,%22selectedSubCategory%22:1,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - ecars: Mathematical approximation of a daily profile f(t) = 0.3 * sin( (t - 55) / 17.3 ) + 0.2 * sin( (t - 30.2) / 7 ) + 0.48

All other data are rough estimations based on the general range of technoeconomical data found in energy system models.

For a comprehensive explanation of all SIMPLE models and corresponding options head over to the 
[best practice guide](https://gitlab.com/beam-me/bpg/-/blob/master/EnergySystemModel_SpeedUp_BestPracticGuide.pdf).


## Building the annotated gdx file

By default the parameters are set up to solve the model as a standard LP. To use SIMPLE with the PIPS-IPM++ solver some
additional steps are required. First we need to write out the problem matrix including the information on which 
variables and equations belong to which block. This can be achieved via the command line options

```bash
gams simple4pips.gms --METHOD=PIPS --BLOCKS=-2
```

This call now generated a monolithic gdx file called *allblocks.gdx* containing the full annotated problem.

To check if the annotation was done correctly we provide the checkanno.gms tool which can be used by specifying the
gdx file we just created.

```bash
gams checkanno.gms --jacFileName=allblocks.gdx
```

The checkanno tool will create a new file called *allblocks_stats.csv* which reports some annotation 
statistics of the instance such as the number of blocks, if the annotation was correct, and if the number of linking
elements are small enough for PIPS-IPM++ to handle them.


## Solving the problem with PIPS-IPM++

If you have [PIPS-IPM++ and the GAMS/PIPS-IPM++-Link](https://github.com/NCKempke/PIPS-IPMpp) installed you can split the monolithic gdx file into the files for 
the individual blocks by using the gmschk tool. Since we have indivicual 365 blocks in the annotated problem we need to 
specify 366 blocks to account for the linking block *allblocks0.gdx*. Additionally we need to specify the
directory in which GAMS is installed (here referenced as a environment variable GAMS_DIR)


```bash
gmschk -s -T -w -X -g $GAMS_DIR 366 allblocks.gdx
```

This created the distributed gdx files *allblocks[0-365].gdx*. To have PIPS-IPM++ solve the problem we need to 
specify the number of blocks as well as the stem of the filename in a MPI environment with MPI processes equal to
the number of individual blocks without the linking block.

```bash
mpirun -n 365 gmspips 366 allblocks $GAMS_DIR scaleGeo presolve stepLp
```

The following figure presents the general workflow of solving optimimization problems with PIPS-IPM++ compared to
solving them with non parallel algorithms.

![Workflow for the PIPS-IPM++ solver](fig/workflow.png?raw=true "Title")


## Block-wise generation of the gdx files

The same imput for the gmspips call can be generated in a parallel way by having simple4pips create the individual
blocks either sequentially or in parallel. The sequential generation can be achieved by teh following call 

```bash
gams simple4pips.gms --METHOD=PIPS --BLOCKS=-1
```

Similarly the individual blocks can be speficied by using the integer value for the corresponding block. This also 
allows the parallel generation of blocks by setting the blocks argument to the MPI rank.

```bash
gams simple4pips.gms --METHOD=PIPS --BLOCKS=[0-365]
```


## Authors
- Frederik Fiand (GAMS Software GmbH)
- Manuel Wetzel (German Aerospace Center)
- Michael Bussieck (GAMS Software GmbH)


## Acknowledgements
This research is part of the BEAM-ME project. It was funded by the German Federal Ministry for Economic Affairs and 
Energy under grant number FKZ 03ET4023A.
